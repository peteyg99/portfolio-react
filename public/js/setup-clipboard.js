function setupClipboard(selector) {
    var clipboard = new Clipboard(selector)

    clipboard.on('success', function(e) {
        e.trigger.parentElement.blur()
        e.clearSelection()
    })
}
