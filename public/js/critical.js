// Add class to body once dom (& js) loaded
var addJsLoadedClass = function()
{
    var body = document.getElementsByTagName("body");
    if (body.length > 0)
    {
        body[0].className = 'critical-js-loaded';
        return true;
    }
    return false;
}

var domLoaded = addJsLoadedClass();

if (!domLoaded)
{
    document.addEventListener("DOMContentLoaded", function(event) { 
        addJsLoadedClass();
    });
}