# Portfolio Site #

This project is built using React, and using NodeJs/Express for server-side rendering.

### Installation ###

* Clone repository
* npm install


### Basic implementation ###

* Rename public/index-static.html to public/index.html
* Serve up site from public/ folder


### Server-side rendering (dev) ###

* Run 'gulp start' in root folder


### Server-side rendering (live) ###

* Run private/react-server.min.js

### Run unit tests ###

* Run 'npm test'