
export const EMAIL = 'email'
export const TEL = 'tel'


export const URL = {
    Resume: 'https://docs.google.com/document/d/1Mw96MhCYmhzianDNRHHW3-_oRS40ueWlwnPmpyQOgiw/edit?usp=sharing',
    ResumePdf: '/files/PeteGaulton-Resume.pdf',
    LinkedIn: 'https://www.linkedin.com/in/pete-gaulton',
    RichmondGroup: 'http://www.therichmondgroup.co.uk',
    AmigoStore: 'https://www.amigostore.co.uk',
    AmigoLoans: 'https://www.amigoloans.co.uk',
    SpotifyBlog: 'http://spotify.gaulton.me',
    MyRepo: 'https://bitbucket.org/peteyg99/',
    SpotifyBlogRepo: 'https://bitbucket.org/peteyg99/spotify-blog',
    StackOverflow: 'http://stackoverflow.com/story/pete-gaulton'
}

export const CONTACT = {
    Email: 'pete@gaulton.me',
    Tel: '+447779093431'
}
