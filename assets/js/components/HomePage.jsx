import React from 'react'
import { Link } from 'react-router'
import FadeInImage from '../components/FadeInImage'
import SlidingHeadline from '../components/SlidingHeadline'


export default class HomePage extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            logos: [
                this.makeLogo("NodeJs", 'nodejs.png'),
                this.makeLogo("React", 'react.png'),
                this.makeLogo("AngularJs", 'angularjs.png'),
                this.makeLogo("REST API", 'rest-api.png'),
                this.makeLogo("JQuery", 'jquery.png'),
                this.makeLogo("Xamarin", 'xamarin.png'),
                this.makeLogo("C#", 'csharp.png'),
                this.makeLogo("PHP", 'php.png'),
                this.makeLogo("SQL", 'sql.png'),
                this.makeLogo("HTML5 CSS3", 'html5css3.svg'),
                this.makeLogo("Laravel", 'laravel.png'),
                this.makeLogo("Code Igniter", 'codeigniter.svg'),
                this.makeLogo("Twilio", 'twilio.png'),
                this.makeLogo("Drupal", 'drupal.png'),
                this.makeLogo("Wordpress", 'wordpress.png')
            ]
        }
    }

    makeLogo(title, img){
        return {
            title,
            key: img,
            imgPath: '/img/logos/' + img
        }
    }

    render(){
        return (
            <div className="homepage">

                <section className="layout-1">
                    <div className="container">
                        <div>
                            <FadeInImage imgPath="/img/profile-main.jpg" title="Pete Gaulton profile pic" />
                        </div>
                        <div>
                            <h1 className="text-muted">
                                Pete Gaulton
                            </h1>
                            <h2>
                                Web/software developer
                            </h2>
                            <Link className="btn" to="/contact">
                                <span className="fa fa-envelope"></span> Get in touch
                            </Link>

                        </div>
                    </div>
                </section>

                <section data-area="logo-section">
                    <div className="container">
                        <SlidingHeadline message="A few things I work with..." />
                        <div className="grid-layout">
                            {this.state.logos.map(logoObj => (
                                <div key={logoObj.key}>
                                    <FadeInImage imgPath={logoObj.imgPath} title={logoObj.title} />
                                </div>
                            ))}
                        </div>
                    </div>
                </section>

            </div>
        )
    }
}
