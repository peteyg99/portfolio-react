import React from 'react'
import ViewportElement from './ViewportElement'

export default class SlideInImage extends ViewportElement {
    constructor(props) {
        super(props)
    }

    render() {
        let inViewClass = this.getElementClass("slide-in-" + this.props.direction)
        return (
            <img className={inViewClass} src={this.props.imgPath} alt={this.props.title} />
        )
    }
}