import React from 'react'
import { URL, CONTACT } from '../constants'
import PageLink from '../components/PageLink'

export default class ContactPage extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const mailToLink = 'mailto:' + CONTACT.Email

        return (
            <div className="contact-page">

                <section className="title-section">
                    <div className="container">
                        <h1>Contact</h1>
                    </div>
                </section>

                <section className="layout-1 fixed-width-cols">
                    <div className="container">
                        <div>
                            <p>
                                Thank you for taking the time view my portfolio. If you have any questions at all
                                please feel free to get in touch.
                            </p>
                            <ul>
                                <PageLink linkUrl={URL.ResumePdf} linkTitle="Resume (PDF)" iconType="download" />
                                <PageLink linkUrl={URL.StackOverflow} linkTitle="Resume (Stack Overflow)" />
                                <PageLink linkUrl={URL.LinkedIn} linkTitle="LinkedIn Profile" />
                            </ul>
                        </div>
                        <div>
                            <dl>
                                <dt>
                                    Email&nbsp;
                                    <a className="btn-inline" href={mailToLink}>
                                        <span className="fa fa-envelope"></span>
                                    </a>
                                </dt> 
                                <dd>
                                    {CONTACT.Email}
                                </dd>
                                <dt>
                                    Tel&nbsp;
                                </dt> 
                                <dd>
                                    {CONTACT.Tel} (UK)
                                </dd>
                            </dl>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

