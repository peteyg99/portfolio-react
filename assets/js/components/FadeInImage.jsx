import React from 'react'
import ViewportElement from './ViewportElement'

export default class FadeInImage extends ViewportElement {
    constructor(props) {
        super(props)
    }

    render() {
        let inViewClass = this.getElementClass("fade-in")
        return (
            <img className={inViewClass} src={this.props.imgPath} alt={this.props.title} />
        )
    }
}