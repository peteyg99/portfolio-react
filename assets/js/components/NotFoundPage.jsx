import React from 'react'
import { Link } from 'react-router'

export default class NotFoundPage extends React.Component {
  render() {
    return (
      <section className="not-found">
        <h1 className="text-muted">404</h1>
        <h2>Page not found</h2>
        <p>
          <Link to="/">Go back to the main page</Link>
        </p>
      </section>
    )
  }
}
