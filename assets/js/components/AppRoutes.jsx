import React from 'react'
import { Router, browserHistory } from 'react-router'
import routes from '../routes'
import ReactGA from 'react-ga'

export default class AppRoutes extends React.Component {

  componentDidMount() {
    ReactGA.initialize('UA-86876611-1')
  }  

  _onUpdate() {
    window.scrollTo(0, 0)
    ReactGA.pageview(browserHistory.getCurrentLocation().pathname)
  }

  render() {
    return (
      <Router history={browserHistory} routes={routes} onUpdate={this._onUpdate}/>
    )
  }
}
