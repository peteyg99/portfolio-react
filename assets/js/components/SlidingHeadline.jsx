import React from 'react'
import ViewportElement from './ViewportElement'

export default class SlidingHeadline extends ViewportElement {
    constructor(props) {
        super(props)
    }

    render() {
        let inViewClass = this.getElementClass("slide-in-left text-muted")
        if (this.state.inViewport) inViewClass += " in-view"
        return (
            <h2 className={inViewClass}>{this.props.message}</h2>
        )
    }
}