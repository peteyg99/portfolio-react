import React from 'react'
import { Link } from 'react-router'
import { URL } from '../constants'
import SlideInImage from '../components/SlideInImage'
import PageLink from '../components/PageLink'

export default class WorkPage extends React.Component {
    constructor(props) {
        super(props)
    }

    render(){
        return (
            <div className="work-page">

                <section className="title-section">
                    <div className="container">
                        <h1>Work</h1>
                    </div>
                </section>

                <section className="layout-1">
                    <div className="container">
                        <div className="copy-col">
                            <h3>Richmond Group</h3>
                            <p>
                                Over the past 4 years I have worked on a number of different projects, 
                                including the customer facing websites, mobile apps (Android & iOS, using 
                                Xamarin), setup of the Twilio call centre for over 250 agents, the feature 
                                rich company intranet, cafe POS, API integrations such as card 
                                auth/payment system, and internal systems that employees use to run 
                                day-to-day operations.
                            </p>
                            <ul>
                                <PageLink linkUrl={URL.RichmondGroup} linkTitle="Richmond Group" />
                                <PageLink linkUrl={URL.AmigoStore} linkTitle="Amigo Store" />
                                <PageLink linkUrl={URL.AmigoLoans} linkTitle="Amigo Loans" />
                            </ul>                        
                        </div>

                        <div className="img-col">
                            <SlideInImage imgPath="/img/screens/amigo_side2-min.png" title="Amigo Loans mobile app screenshot" direction="right" />
                        </div>

                    </div>
                </section>

                <section className="layout-1">
                    <div className="container">
                        <div className="img-col">
                            <SlideInImage imgPath="/img/screens/spotify_side1-min.png" title="Spotify blog mobile screenshot" direction="left" />
                        </div>
                        <div className="copy-col">
                            <h3>My Spotify Blog</h3>
                            <p>
                                This is an example web app that I put together to share my Spotify
                                playlists with friends, built using React Redux and Node/Express for 
                                server-side rendering.
                            </p>
                            <ul>
                                <PageLink linkUrl={URL.SpotifyBlog} linkTitle="My Spotify Blog" />
                                <PageLink linkUrl={URL.SpotifyBlogRepo} linkTitle="Repository" />
                            </ul>                            
                        </div>
                    </div>
                </section>

                <section className="layout-1 fixed-width-cols">
                    <div className="container">
                        <div>
                            <h3>Bitbucket</h3>
                            <p>
                                Here's a link to my Git repository, with a few boilerplate projects, 
                                plus the code for this site and my Spotify blog. More projects coming soon.
                            </p>
                            <ul>
                                <PageLink linkUrl={URL.MyRepo} linkTitle="Bitbucket Repository" />
                            </ul>
                        </div>
                        <div>
                            <h3>Resume</h3>
                            <p>
                                Download a copy of my resume here, if you have any questions at all please <Link to="/contact">get in touch</Link>.
                            </p>
                            <ul>
                                <PageLink linkUrl={URL.ResumePdf} linkTitle="Resume (PDF)" iconType="download" />
                                <PageLink linkUrl={URL.StackOverflow} linkTitle="Resume (Stack Overflow)" />
                            </ul>
                        </div>
                    </div>
                </section>

            </div>
        )
    }
}

