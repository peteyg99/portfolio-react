import React from 'react'
import inViewport from 'in-viewport'
import ReactDOM from 'react-dom'

export default class ViewportElement extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            inViewport: false
        }
    }

    componentDidMount() {
        // watch for viewport entry
        const el = ReactDOM.findDOMNode(this)
        this.watcher = inViewport(el, () => {
            this.setState({
                inViewport: true
            })
        })
    }
    
    componentWillUnmount() {
        if (this.watcher) {
            this.watcher.dispose()
        }
    }

    getElementClass(defaultClass) {
        if (this.state.inViewport) defaultClass += " in-view"
        return defaultClass
    }

}