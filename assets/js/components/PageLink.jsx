import React, { PropTypes } from 'react'

class PageLink extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let { linkUrl, linkTitle, iconType } = this.props

        const iconClass = "fa fa-" + iconType
        return (
            <li>
                <a target="_blank" href={ linkUrl }>
                    <span className={ iconClass }></span>
                    &nbsp;&nbsp;{ linkTitle }
                </a>
            </li>
        )
    }
}

PageLink.defaultProps = {
  iconType: "external-link"
};

PageLink.propTypes = {
  linkUrl: PropTypes.string.isRequired,
  linkTitle: PropTypes.string.isRequired,
}

export default PageLink