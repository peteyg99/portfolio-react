import React from 'react'
import { browserHistory } from 'react-router'
import { Link } from 'react-router'

export default class Layout extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            menuOpen: false
        }
        this.onClick = this.onClick.bind(this)

        if (!browserHistory) return
        browserHistory.listen(ev => {
            this.updateMenu(false)
        })
    }

    onClick(){
        this.updateMenu(!this.state.menuOpen)
    }

    updateMenu(openMenu){
        this.setState({
            menuOpen: openMenu
        })
    }

    navClass(){
        return this.state.menuOpen ? "nav-open" : ""
    }

    render(){
        return (
            <div id="layout-wrap">
                <div className="test-header-wrap">
                    <div className="test-header">
                        <div className="test-header-child-1"></div>
                        <div className="test-header-child-2"></div>
                    </div>
                </div>


                <header>
                    <div className="container">
                        <div className="main-header">
                            <Link to="/" className="nav-brand">Portfolio</Link>
                            <button type="button" className="nav-toggle" onClick={this.onClick}>
                                <span className="fa fa-ellipsis-h"></span>
                            </button> 
                        </div>

                        <nav className={this.navClass()}>
                            <ul>
                                <li><Link to="/">Home</Link></li>
                                <li><Link to="/work">Work</Link></li>
                                <li><Link to="/contact">Contact</Link></li>
                            </ul>
                        </nav>
                    </div>
                </header>
                <main>
                    {this.props.children}
                </main>
                <footer>
                    <div className="container">
                    </div>
                </footer>
            </div>
        )
    }
}


