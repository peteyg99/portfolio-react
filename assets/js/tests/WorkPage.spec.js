import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'

import WorkPage from '../components/WorkPage'
import PageLink from '../components/PageLink'
import { URL } from '../constants'

function setup() {
    return shallow(<WorkPage />)
}

describe('<WorkPage />', () => {

    it('should have a link to MyRepo', function () {
        const enzymeWrapper = setup()
        const link = enzymeWrapper.find({ linkUrl: URL.MyRepo })
        expect(link).to.have.length(1)
    })
    it('should have a link to resume', function () {
        const enzymeWrapper = setup()
        const link = enzymeWrapper.find({ linkUrl: URL.ResumePdf })
        expect(link).to.have.length(1)
    })
    it('should have a link to StackOverflow', function () {
        const enzymeWrapper = setup()
        const link = enzymeWrapper.find({ linkUrl: URL.StackOverflow })
        expect(link).to.have.length(1)
    })
})
