import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'

import HomePage from '../components/HomePage'
import FadeInImage from '../components/FadeInImage'


function setup() {
    return shallow(<HomePage />)
}

describe('<HomePage />', () => {

    it('should contain 15 logos', function () {
        const enzymeWrapper = setup()
        const logoSection = enzymeWrapper.find('section[data-area="logo-section"]')
        expect(logoSection.find(FadeInImage)).to.have.length(15)
    })
    it('should have a link to contact page', function () {
        const enzymeWrapper = setup()
        const contactLink = enzymeWrapper.find({ to: "/contact" })
        expect(contactLink).to.have.length(1)
    })
    it('should contain my name in <h1>', function () {
        const enzymeWrapper = setup()
        expect(enzymeWrapper.find('h1').text()).to.contain("Pete Gaulton")
    })
})
