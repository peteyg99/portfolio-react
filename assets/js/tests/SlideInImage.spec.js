import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'

import SlideInImage from '../components/SlideInImage'

function setup(props) {
    return shallow(<SlideInImage {...props} />)
}

describe('<SlideInImage />', () => {

    it('should have 1 <img>', function () {
        const enzymeWrapper = setup()
        expect(enzymeWrapper.find('img')).to.have.length(1)
    })

    it('should have className containing `slide-in`', function () {
        const enzymeWrapper = setup()
        expect(enzymeWrapper.find('img').props().className).to.contain("slide-in")
    })

    it('should add direction to className', function () {
        const direction = "test"
        const enzymeWrapper = setup({ direction })
        expect(enzymeWrapper.find('img').props().className).to.contain(direction)
    })

    it('should update class to `in-view` when in viewport', function () {
        const enzymeWrapper = setup()
        expect(enzymeWrapper.find('img').props().className).to.not.contain('in-view')
        enzymeWrapper.setState({ inViewport: true })
        expect(enzymeWrapper.find('img').props().className).to.contain('in-view')
    })
})
