import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'

import NotFoundPage from '../components/NotFoundPage'

function setup() {
    return shallow(<NotFoundPage />)
}

describe('<NotFoundPage />', () => {

    it('should have a link to root page', function () {
        const enzymeWrapper = setup()
        const link = enzymeWrapper.find({ to: '/' })
        expect(link).to.have.length(1)
    })
})
