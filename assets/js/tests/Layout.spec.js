import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'

import Layout from '../components/Layout'
import { URL } from '../constants'

function setup() {
    return shallow(<Layout />)
}

function checkLinkExists(linkPath) {
    const enzymeWrapper = setup()
    const link = enzymeWrapper.find({ to: linkPath })
    expect(link.length).to.be.greaterThan(0)
}

describe('<Layout />', () => {
    it('should have at least one link to root page', function () {
        checkLinkExists('/')
    })
    it('should have at least one link to work page', function () {
        checkLinkExists('/work')
    })
    it('should have at least one link to contact page', function () {
        checkLinkExists('/contact')
    })
})