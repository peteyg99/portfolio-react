import React from 'react'
import { mount } from 'enzyme'
import { expect, assert } from 'chai'

import PageLink from '../components/PageLink'

function setup(testProps) {
    const props = {
        linkUrl: "http://www.test.com",
        linkTitle: "Test link",
        ...testProps
    }

    const error = console.error;
    console.error = function(warning, ...args) {
        if (/(Invalid prop|Failed prop type)/.test(warning)) {
            throw new Error(warning)
        }
        error.apply(console, [warning, ...args])
    }

    const enzymeWrapper = mount(
        <PageLink {...props} />
    )

    const link = enzymeWrapper.find('a')
    const span = enzymeWrapper.find('span')

    return {
        props,
        enzymeWrapper,
        link,
        span
    }
}

describe('<PageLink />', () => {
    it('should have a default iconType', function () {
        const { enzymeWrapper } = setup()
        expect(enzymeWrapper.props().iconType).to.be.a('string')
    })
    it('should be able to set iconType', function () {
        const iconType = "update-test"
        const { enzymeWrapper } = setup({ iconType })
        expect(enzymeWrapper.props().iconType).to.equal(iconType)
    })
    it('should have an icon with class containing iconType', function () {
        const iconType = "update-test"
        const { span } = setup({ iconType })
        expect(span.props().className).to.contain(iconType)
    })

    it('should contain an <li>', function () {
        const { enzymeWrapper } = setup()
        expect(enzymeWrapper.find('li')).to.have.length(1)
    })
    it('should contain 1 <a>', function () {
        const { link } = setup()
        expect(link).to.have.length(1)
    })
    it('should have a link URL', function () {
        const { props, link } = setup()
        expect(link.props().href).to.equal(props.linkUrl)
    })
    it('should have a link title', function () {
        const { props, link } = setup()
        expect(link.text()).to.contain(props.linkTitle)
    })

    it('should throw error if no link URL', function () {
        assert.throws(function(){
            setup({ linkUrl: null })
        }, Error)
    })
    it('should throw error if no link title', function () {
        assert.throws(function(){
            setup({ linkTitle: null })
        }, Error)
    })
})
