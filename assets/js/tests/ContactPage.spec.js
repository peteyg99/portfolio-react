import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'

import ContactPage from '../components/ContactPage'
import PageLink from '../components/PageLink'
import { CONTACT } from '../constants'

function setup() {
    return shallow(<ContactPage />)
}

describe('<ContactPage />', () => {

    it('should contain 3 <PageLink>', function () {
        const enzymeWrapper = setup()
        expect(enzymeWrapper.find(PageLink)).to.have.length(3)
    })
    it('should contain my email address', function () {
        const enzymeWrapper = setup()
        expect(enzymeWrapper.find('dl').text()).to.contain(CONTACT.Email)
    })
    it('should contain my telephone number', function () {
        const enzymeWrapper = setup()
        expect(enzymeWrapper.find('dl').text()).to.contain(CONTACT.Tel)
    })
})
