import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'

import SlidingHeadline from '../components/SlidingHeadline'

function setup(props) {
    return shallow(<SlidingHeadline {...props} />)
}

describe('<SlidingHeadline />', () => {

    it('should have 1 <h2>', function () {
        const enzymeWrapper = setup()
        expect(enzymeWrapper.find('h2')).to.have.length(1)
    })

    it('should have className containing `slide-in`', function () {
        const enzymeWrapper = setup()
        expect(enzymeWrapper.find('h2').props().className).to.contain("slide-in")
    })

    it('should update class to `in-view` when in viewport', function () {
        const enzymeWrapper = setup()
        expect(enzymeWrapper.find('h2').props().className).to.not.contain('in-view')
        enzymeWrapper.setState({ inViewport: true })
        expect(enzymeWrapper.find('h2').props().className).to.contain('in-view')
    })
})
