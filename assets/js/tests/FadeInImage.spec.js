import React from 'react'
import { shallow } from 'enzyme'
import { expect } from 'chai'

import FadeInImage from '../components/FadeInImage'

function setup(props) {
    return shallow(<FadeInImage {...props} />)
}

describe('<FadeInImage />', () => {

    it('should have 1 <img>', function () {
        const enzymeWrapper = setup()
        expect(enzymeWrapper.find('img')).to.have.length(1)
    })

    it('should have className containing `fade-in`', function () {
        const enzymeWrapper = setup()
        expect(enzymeWrapper.find('img').props().className).to.contain("fade-in")
    })

    it('should update class to `in-view` when in viewport', function () {
        const enzymeWrapper = setup()
        expect(enzymeWrapper.find('img').props().className).to.not.contain('in-view')
        enzymeWrapper.setState({ inViewport: true })
        expect(enzymeWrapper.find('img').props().className).to.contain('in-view')
    })
})
