'use strict';

import React from 'react'
import { Route, IndexRoute } from 'react-router'
import Layout from './components/Layout'
import HomePage from './components/HomePage'
import WorkPage from './components/WorkPage'
import ContactPage from './components/ContactPage'
import NotFoundPage from './components/NotFoundPage'

const routes = (
  <Route path="/" component={Layout}>
    <IndexRoute component={HomePage}/>
    <Route path="/work" component={WorkPage}/>
    <Route path="/contact" component={ContactPage}/>
    <Route path="*" component={NotFoundPage}/>
  </Route>
);

export default routes;
