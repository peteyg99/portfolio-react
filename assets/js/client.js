import React from 'react'
import ReactDOM from 'react-dom'

import AppRoutes from './components/AppRoutes'


(function() {
    "use strict"
    const appRoot = document.getElementById("root")
    ReactDOM.render(React.createElement(AppRoutes), appRoot)
})()