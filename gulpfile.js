'use strict'

var gulp = require('gulp'),
    del = require('del'),
    rename = require('gulp-rename'),
    less = require('gulp-less'),
    cssmin = require('gulp-cssmin'),
    react = require('gulp-react'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    nodemon = require('gulp-nodemon')
 

var browserifyConfig = {
    extensions: ['.js', '.jsx'],
    debug: !isProduction(),
}
var browserifySvrConfig = {
    extensions: ['.js', '.jsx'],
    debug: !isProduction(),
    bare: true,
    browserField: false,
    insertGlobalVars: {
        process: function() { return; }
    },
    builtins: false,
    commondir: false
}

var config = {
    lessSrcFile: 'site.less',
    lessSrcPath: './assets/less/',
    lessOutPath: './public/css/',
    jsOutFilePrefix: 'react-',
    jsSrcPath: './assets/js/',
    client: {
        jsOutPath: './public/js/',
        jsSrcFile: 'client.js',
        browserify: browserifyConfig
    },
    server: {
        jsOutPath: './private/',
        jsSrcFile: 'server.js',
        browserify: browserifySvrConfig
    }
}
config.lessSrcFilePath = config.lessSrcPath + config.lessSrcFile
config.cleanLessPath = config.lessOutPath + '*.*'

config.jsCleanPath = config.jsOutFilePrefix + '*.*'

process.env.NODE_ENV = 'production'


gulp.task('default', ['push-public', 'bundle:js', 'bundle:js-server', 'bundle:less'])

gulp.task('set-dev', function() {
    process.env.NODE_ENV = 'development'
    gulp.watch(config.lessSrcPath + '**/*.less', ['bundle:less'])
    gulp.watch(config.jsSrcPath + '**/*.js*', ['bundle:js', 'bundle:js-server'])
})

gulp.task('dev', ['set-dev', 'default'])

gulp.task('start', ['dev', 'bundle:js-server'], function () {
  nodemon({
    script: config.server.jsOutPath + config.jsOutFilePrefix + config.server.jsSrcFile,
    ext: 'js html',
    env: { 'NODE_ENV': process.env.NODE_ENV }
  })
})

gulp.task('bundle:less', function() {
    del(config.cleanLessPath)

    return gulp.src(config.lessSrcFilePath)
            .pipe(less())
            .pipe(gulp.dest(config.lessOutPath))
            .pipe(cssmin())
            .pipe(rename({ suffix: '.min' }))
            .pipe(gulp.dest(config.lessOutPath))

})

gulp.task('push-public', function() {
    gulp.src('node_modules/clipboard/dist/clipboard.min.js')
        .pipe(gulp.dest('public/js'))
    return gulp.src('node_modules/font-awesome/fonts/*')
        .pipe(gulp.dest('public/fonts'))
})

gulp.task('bundle:js', function() {
    return bundleJs(config.client)
})

gulp.task('bundle:js-server', function() {
    return bundleJs(config.server)
})

function bundleJs(srcType) {
    del(srcType.jsOutPath + config.jsCleanPath)

    return browserify(
        config.jsSrcPath + srcType.jsSrcFile, 
        srcType.browserify
    )
    .transform(babelify, {
        presets: ['es2015', 'react'],
        plugins: ['transform-object-rest-spread']
    })
    .bundle()
    .on("error", function (err) { 
        console.log("Error : " + err.message) 
        if (!isProduction()) this.emit('end')
    })
    .pipe(source(srcType.jsSrcFile))
    .pipe(buffer())
    .pipe(rename({ prefix: config.jsOutFilePrefix, extname: '.js' }))
    .pipe(gulp.dest(srcType.jsOutPath))
    .pipe(uglify({ compress: { unused: false } }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(srcType.jsOutPath))

}


function isProduction() {
    return process.env.NODE_ENV === 'production'
}